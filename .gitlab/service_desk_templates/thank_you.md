%{ISSUE_ID} - Issue created

Thank you for your e-mail. We have created an Issue under %{ISSUE_PATH}.
You will get notified of any further comments and changes concerning your issue.

Sincerely,
The Support Team
